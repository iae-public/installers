tput setaf 5
echo -e "INSTALL MOSQUITTO" 
tput setaf 7 

sudo apt-add-repository ppa:mosquitto-dev/mosquitto-ppa

sudo apt-get update

sudo apt-get install mosquitto

sudo service mosquitto status

sudo service mosquitto stop

tput setaf 5
echo -e "SET PASSWORD" 
tput setaf 7 

sudo mosquitto_passwd -c -b /etc/mosquitto/passwd %1 %2

tput setaf 5
echo -e "SET CONFIGURATION" 
tput setaf 7 

# Configure programs
sudo echo "# by FV@iae
pid_file /var/run/moquitto.pid

port 1883

persistence false
log_type none

password_file /etc/mosquitto/passwd
allow_anonymous false

listener 9001
protocol websockets" > /etc/mosquitto/mosquitto.conf

tput setaf 5
echo -e "THEN RESTART SYSTEM ( sudo reboot )" 
tput setaf 7 
