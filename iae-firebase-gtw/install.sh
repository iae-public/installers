sudo apt-get update

sudo apt-get install -y build-essential

sudo apt-get install -y git

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo npm i -g npm 
sudo npm i -g typescript
sudo npm i -g pm2

cd /usr/bin/
sudo git clone https://gitlab.com/iae-iot/iae-firebase-gtw.git

cd /usr/bin/iae-firebase-gtw

sudo npm i
sudo tsc

sudo pm2 start build/index.js --name iae-firebase-gtw
sudo pm2 startup
sudo pm2 save







