### Install
```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/iae4j-appliance/install.sh | sudo -E bash -
```

### Update
```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/iae4j-appliance/update.sh | sudo -E bash -
```

### Update Only Project (no dependencies changed)
```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/iae4j-appliance/update.only.prj.sh | sudo -E bash -
```