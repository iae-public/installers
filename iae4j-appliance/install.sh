sudo tput setaf 3 
sudo echo -e "\n(1/5) >>> SETUP SYSTEM\n"
sudo tput setaf 7 

sudo apt-get update
#sudo apt-get upgrade

sudo apt-get install -y build-essential
sudo apt-get install -y git
sudo apt-get install -y python-dev

curl -sL https://deb.nodesource.com/setup_11.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo npm i -g npm
sudo npm i -g typescript pm2

sudo tput setaf 3
sudo echo -e "\n(2/5) >>> DOWNLOAD PROJECT\n"
sudo tput setaf 7 

cd /usr/bin/
sudo git clone --recursive https://gitlab.com/iae-iot/iae4j-appliance.git

cd /usr/bin/iae4j-appliance

sudo tput setaf 3
sudo echo -e "\n(3/5) >>> DOWNLOAD DEPENDENCIES\n"
sudo tput setaf 7 

sudo npm i

sudo tput setaf 3
sudo echo -e "\n(4/5) >>> COMPILE PROJECT\n"
sudo tput setaf 7 

sudo tsc

sudo mkdir -p build/local-data

sudo tput setaf 3
sudo echo -e "\n(5/5) >>> SETUP PM2\n"
sudo tput setaf 7 

sudo pm2 start ecosystem.config.js
sudo tput setab 0 # riporto il bg a nero

sudo pm2 startup
sudo tput setab 0 # riporto il bg a nero

sudo pm2 save
sudo tput setab 0 # riporto il bg a nero

sudo tput setaf 5
sudo echo -e "\nGATEWAY STARTED WITH GUID: 00000000-0000-0000-0000-000000000000" 
sudo echo -e "PUT RIGHT GUID INTO DM.JSON FILE ( sudo nano /usr/bin/iae4j-appliance/build/local-data/dm.json )" 
sudo echo -e "THEN RESTART SYSTEM ( sudo reboot )" 
sudo tput setaf 7 
