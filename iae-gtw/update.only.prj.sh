sudo tput setaf 11
sudo echo -e "\n(1/3) >>> UPDATE PROJECT\n"
sudo tput setaf 7

cd /home/pi/iae-gtw
    
sudo git checkout v3
sudo git pull origin v3

sudo tput setaf 11
sudo echo -e "\n(2/3) >>> COMPILE PROJECT\n"
sudo tput setaf 7

sudo tsc

sudo tput setaf 11
sudo echo -e "\n(3/3) >>> RESTART PROCESS\n"
sudo tput setaf 7

sudo pm2 restart iae-gtw
sudo tput setab 0 # riporto il bg a nero

sudo tput setaf 5
sudo echo -e "\nDONE" 
sudo tput setaf 7
