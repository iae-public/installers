sudo tput setaf 3
sudo echo -e "\n(1/5) >>> SETUP SYSTEM\n"
sudo tput setaf 7

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common git jq
sudo apt-get install -y build-essential
sudo apt-get install -y git
sudo apt-get install -y python-dev

curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo npm i -g npm
sudo npm i -g typescript pm2

sudo tput setaf 3
sudo echo -e "\n(2/5) >>> DOWNLOAD PROJECT\n"
sudo tput setaf 7

cd /home/pi/
sudo git clone -b v3 --recursive http://gitlab+deploy-token-68659:$1@gitlab.com/iae-iot/iae-gtw.git
cd iae-gtw

sudo tput setaf 3
sudo echo -e "\n(3/5) >>> DOWNLOAD DEPENDENCIES\n"
sudo tput setaf 7

sudo npm i --unsafe-perm

sudo tput setaf 3
sudo echo -e "\n(4/5) >>> COMPILE PROJECT\n"
sudo tput setaf 7

sudo tsc

sudo chown -R $USER .
jq ".gatewayUid = \"$2\"" <<< `cat blank-dm.json` > build/local-data/dm.json
jq ".gateway.name = \"`hostname`\"" <<<`cat blank-config.json` > build/local-data/config.json

sudo tput setaf 3
sudo echo -e "\n(5/5) >>> SETUP PM2\n"
sudo tput setaf 7

sudo pm2 start build/index.js --name iae-gtw
sudo tput setab 0 # riporto il bg a nero

sudo pm2 startup
sudo tput setab 0 # riporto il bg a nero

sudo pm2 save
sudo tput setab 0 # riporto il bg a nero
