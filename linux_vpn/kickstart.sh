sudo tput setaf 11 
sudo echo -e "\n(1/3) >>> SCARICO IL FILE DAL TEMP STORE\n"
sudo tput setaf 7 

curl -o /home/pi/$1.ovpn -sL https://iae-device-manager.azurewebsites.net/api/main/store/$2

cd /home/pi
sudo mv --force $1.ovpn /

sudo tput setaf 11 
sudo echo -e "\n(2/3) >>> INSTALLO IL CLIENT VPN\n"
sudo tput setaf 7 

sudo apt-get update -y
#sudo apt-get update && sudo apt-get dist-upgrade -y
sudo apt-get install -y openvpn

sudo tput setaf 11 
sudo echo -e "\n(3/3) >>> CONFIGURO IL CLIENT VPN\n"
sudo tput setaf 7 

sudo echo "# CLIENT VPN
[Unit]
Description=OpenVPN IAE tunnel
After=syslog.target network.target

[Service]
Type=simple
ExecStart=/usr/sbin/openvpn --config /$1.ovpn
Restart=on-failure
User=root
Group=root

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/openvpn.service

sudo systemctl enable openvpn
sudo systemctl start openvpn
