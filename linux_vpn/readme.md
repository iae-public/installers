# VPN

> nel server vpn censisco il client con il seguente comando (verrà chiesto il nome del client)

```sh
pivpn -a [nopass]
```

> trasferisco il file sul temp store con:

```sh
curl -sL https://gitlab.com/iae-public/installers/raw/master/linux_vpn/transfer.sh | sudo -E bash -s {USR} {NOME-CLIENT} {ID}
```

> questo comando restituisce un CURL da usare nel client (installa il client vpn, scarica il file dal temp store e configura il client)

```sh
curl -sL https://gitlab.com/iae-public/installers/raw/master/linux_vpn/kickstart.sh | sudo -E bash -s {NOME-CLIENT} {ID}
```

