sudo tput setaf 11
sudo echo -e "\n(1/5) >>> UPDATE SYSTEM\n"
sudo tput setaf 7

sudo apt-get update

sudo npm i -g npm
sudo npm i -g typescript
sudo npm i -g pm2

sudo tput setaf 11
sudo echo -e "\n(2/5) >>> UPDATE PROJECT\n"
sudo tput setaf 7

cd /home/pi/supervisor
sudo git config credential.helper store
sudo git pull

sudo tput setaf 11
sudo echo -e "\n(3/5) >>> UPDATE DEPENDENCIES\n"
sudo tput setaf 7

sudo npm i

sudo tput setaf 11
sudo echo -e "\n(4/5) >>> COMPILE PROJECT\n"
sudo tput setaf 7

sudo tsc --build tsconfig.json

sudo tput setaf 11
sudo echo -e "\n(5/5) >>> RESTART PROCESS\n"
sudo tput setaf 7

sudo pm2 restart RE-SUPVSR
sudo tput setab 0 # riporto il bg a nero

sudo tput setaf 11
sudo echo -e "\n(END) >>> DONE (sudo reboot)" 
sudo tput setaf 7