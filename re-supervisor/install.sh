tput setaf 11 
echo -e "\n(1/5) >>> SETUP SYSTEM\n"
tput setaf 7 

# Update packages, add nodejs repo and upgrade OS
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get -y dist-upgrade

# install apt dependencies
sudo apt-get install -y build-essential libudev-dev git nodejs fswebcam
#sudo apt-get install -y git nodejs fswebcam
sudo npm i -g npm typescript pm2

tput setaf 11
echo -e "\n(2/5) >>> DOWNLOAD PROJECT\n"
tput setaf 7 

cd /home/pi/
# download source code from git as root
sudo git clone https://gitlab.com/remote-expert/supervisor.git 

# resore ownership as pi
sudo chown -R pi supervisor

cd supervisor

tput setaf 11
echo -e "\n(3/5) >>> INSTALL DEPENDENCIES\n"
tput setaf 7 

sudo npm i

sudo tput setaf 3
sudo echo -e "\n(4/5) >>> BUILD PROJECT\n"
sudo tput setaf 7 

sudo tsc

tput setaf 11
echo -e "\n(5/5) >>> SETUP PM2\n"
tput setaf 7 

# start processes with pm2 as root (root pm2 group)
sudo pm2 start build/index.js --name RE-SUPVSR
sudo tput setab 0 # riporto il bg a nero

# configure pm2 startup
sudo pm2 startup
sudo pm2 save

tput setab 0 # riporto il bg a nero
tput setaf 11
echo -e "\n(FINE) >>> Riavvia il sistema ( sudo reboot )" 
tput setaf 7 
