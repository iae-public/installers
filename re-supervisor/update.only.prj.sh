tput setaf 11
sudo echo -e "<1/3> UPDATE PROJECT"
tput setaf 7

cd /home/pi/supervisor
sudo git config credential.helper store
sudo git pull

tput setaf 11
sudo echo -e "<2/3> COMPILE PROJECT"
tput setaf 7

sudo tsc --build tsconfig.json

tput setaf 11
sudo echo -e "<3/3> RESTART PROCESS"
tput setaf 7

sudo pm2 restart RE-SUPVSR

tput setaf 11
sudo echo -e "<END> DONE ! ( sudo reboot )" 
tput setaf 7
