### HowTo

Permette di connettersi via ssh a device linux passando tramite un server.
Le device sono sempre client ssh del server in modalità reverse-proxy
Lo smistamento avviene tramite la porta del server.


```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/ssh-tun/service.generic.sh | sudo -E bash -s {service-name} {tunnel-port} {local ip:local port} {server-url} {local-user} {server-user} {ssh-server-port}
```

**To use**
```sh
ssh -p {tunnel-port} {local-user}@{server-url}
```