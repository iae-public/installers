tput setaf 3
echo -e ">>> SETUP SYSTEMD (AUTO-START)\n"
tput setaf 7 

sudo echo "[Unit]
Description=IAE SSH Tunnel Service
After=network.target
[Service]
User=$3
ExecStart=/usr/bin/ssh -NT -o ExitOnForwardFailure=yes -o ServerAliveInterval=60 -o ServerAliveCountMax=3 -R $2:localhost:22 -i /home/$3/.ssh/id_rsa $1
RestartSec=15
Restart=always
[Install]
WantedBy=multi-user.target" > /etc/systemd/system/iae-ssh-tunnel.service

sudo systemctl enable iae-ssh-tunnel
sudo systemctl start iae-ssh-tunnel

tput setaf 5
echo -e "\nDONE ! (restart system with 'sudo reboot' to check autostart)\n" 
tput setaf 7 
