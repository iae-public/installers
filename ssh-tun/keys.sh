tput setaf 3
echo -e "\n>>> Answer YES and then insert password for $1\n"
tput setaf 7 

tput setaf 3
echo -e ">>> SSH-KEYGEN for current user: $USER\n"
tput setaf 7 

ssh-keygen -q -N "" -t rsa -f "/home/$USER/.ssh/id_rsa"

tput setaf 3
echo -e ">>> COPY ID TO SERVER\n"
tput setaf 7 

ssh-copy-id $1

tput setaf 3
echo -e ">>> DONE !\n" 
tput setaf 7 
