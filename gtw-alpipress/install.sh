curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
sudo apt-get install -y nodejs git
sudo mkdir -p /usr/bin/
sudo mkdir -p /etc/gateway
cd /usr/bin/
sudo git clone -b sk-2 --recursive https://gitlab.com/iae-alpipress/gateway.git
cd /usr/bin/gateway
sudo npm i
sudo npm i -g typescript
sudo tsc
sudo touch /etc/systemd/system/sk.service
sudo touch /etc/gateway/dm.json
sudo echo "[Unit]
Description=IAE Gateway
Wants=network-online.target
After=network.target network-online.target

[Service]
WorkingDirectory=/usr/bin/gateway/
ExecStart=/usr/bin/node /usr/bin/gateway/build/index.js
Restart=always
RestartSec=10
SyslogIdentifier=iae-gateway
User=root

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/sk.service

sudo echo "{
    \"url\": \"http://iae-device-manager.azurewebsites.net\",
    \"gatewayUid\": \"{guid}\",
    \"configVersion\": \"0.0.0\"
}" > /etc/gateway/dm.json

sudo nano /etc/gateway/dm.json

sudo systemctl enable sk
sudo systemctl start sk
sudo systemctl status sk