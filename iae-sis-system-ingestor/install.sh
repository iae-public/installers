tput setaf 3 
echo -e "\n(1/6) >>> INSTALL DOCKER\n"
tput setaf 7 

sudo apt-get update
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common git jq
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
sudo usermod -aG docker pi
sudo apt install -y docker-compose

tput setaf 3 
echo -e "\n(2/6) >>> SETUP ENVIRONMENT\n"
tput setaf 7 

# Update packages, add nodejs repo and upgrade OS
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt-get -y dist-upgrade

# install apt dependencies
sudo apt-get install -y nodejs
sudo npm i -g npm typescript pm2

tput setaf 3
echo -e "\n(3/6) >>> DOWNLOAD PROJECT\n"
tput setaf 7 

cd /home/pi/
# download source code from git as root
sudo git clone https://gitlab.com/iae-sis-system/sis-system-ingestor.git 

# resore ownership as pi
sudo chown -R pi sis-system-ingestor

cd sis-system-ingestor

tput setaf 3
echo -e "\n(4/6) >>> INSTALL DEPENDENCIES\n"
tput setaf 7 

sudo npm i

sudo tput setaf 3
sudo echo -e "\n(5/6) >>> BUILD PROJECT\n"
sudo tput setaf 7 

sudo tsc

# create local data in build and configure json
mkdir -p build/local-data
jq ".uid = \"$1\"" <<< `cat defaults/default.json` > build/local-data/config.json
jq ".name = \"`hostname`\"" <<<`cat build/local-data/config.json` > build/local-data/config.json

tput setaf 3
echo -e "\n(6/6) >>> SETUP PM2\n"
tput setaf 7 

# start processes with pm2 as root (root pm2 group)
sudo pm2 start build/main.js --name MAIN
sudo tput setab 0 # riporto il bg a nero

# configure pm2 startup
sudo pm2 startup
sudo pm2 save

tput setab 0 # riporto il bg a nero
tput setaf 5
echo -e "\nINGESTOR STARTED WITH GUID: $1" 
tput setaf 7 
