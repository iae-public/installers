sudo echo -e "<1/5> UPDATE SYSTEM"

sudo apt-get update

sudo npm i -g npm
sudo npm i -g typescript
sudo npm i -g pm2

sudo echo -e "<2/5> UPDATE PROJECT"

cd /home/pi/sis-system-ingestor
sudo git config credential.helper store
sudo git pull

sudo echo -e "<3/5> UPDATE DEPENDENCIES"

sudo npm i

sudo echo -e "<4/5> COMPILE PROJECT"

sudo tsc --build tsconfig.json

sudo echo -e "<5/5> RESTART PROCESS"

sudo pm2 restart SIG-ING

sudo echo -e "<DONE>" 