### Prepare
- UBUNTU SERVER (64bit) with user pi

### Install
```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/iae-sis-system-ingestor/install.sh | sudo -E bash -s {GUID}
```

### Update
```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/iae-sis-system-ingestor/update.sh | sudo -E bash -
```

### Update Only Project (no dependecies changed)
```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/iae-sis-system-ingestor/update.only.prj.sh | sudo -E bash -
```