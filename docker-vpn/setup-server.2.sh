cd /home/pi/

tput setaf 11 
echo -e "\n(4/7) >>> docker-compose run --rm openvpn ovpn_initpki\n"
tput setaf 7 

docker-compose run --rm openvpn ovpn_initpki

tput setaf 11 
echo -e "\n(5/7) >>> sudo chown -R pi: ./openvpn-data\n"
tput setaf 7 

sudo chown -R pi: ./openvpn-data

tput setaf 11 
echo -e "\n(6/7) >>> docker-compose up -d openvpn\n"
tput setaf 7 

docker-compose up -d openvpn

tput setaf 11 
echo -e "\n(7/7) >>> docker-compose logs -f\n"
tput setaf 7 

docker-compose logs -f