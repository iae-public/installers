# DOCKER VPN

> setup del server:

```sh
cd /home/{USER}/

sudo echo "version: '2'
services:
  openvpn:
    cap_add:
     - NET_ADMIN
    image: kylemanna/openvpn
    container_name: openvpn
    ports:
     - '1194:1194/udp'
    restart: always
    volumes:
     - ./openvpn-data/conf:/etc/openvpn" > ./docker-compose.yml

docker-compose run --rm openvpn ovpn_genconfig -u udp://{SERVER-VPN-URL}

docker-compose run --rm openvpn ovpn_initpki

sudo chown -R {USER}: ./openvpn-data

docker-compose up -d openvpn

# per visualizzare il log
docker-compose logs -f
```

> preaparazione certificato client (nel server)

```sh
curl -sL https://gitlab.com/iae-public/installers/raw/master/docker-vpn/create-client.sh | sudo -E bash -s {NOME-CLIENT}
```

> setup client openvpn (nel client linux)

```sh
curl -sL https://gitlab.com/iae-public/installers/raw/master/docker-vpn/setup-client.sh | sudo -E bash -s {NOME-CLIENT}
```

> setup client openvpn (nel client windows)

```sh
curl -sL https://gitlab.com/iae-public/installers/raw/master/docker-vpn/download-client.sh | sudo -E bash -s {NOME-CLIENT}
```