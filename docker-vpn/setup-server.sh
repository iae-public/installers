tput setaf 11 
echo -e "\n(1/7) >>> cd /home/pi/\n"
tput setaf 7 

cd /home/pi/

tput setaf 11 
echo -e "\n(2/7) >>> create docker-compose.yml\n"
tput setaf 7 

sudo echo "version: '2'
services:
  openvpn:
    cap_add:
     - NET_ADMIN
    image: kylemanna/openvpn
    container_name: openvpn
    ports:
     - '1194:1194/udp'
    restart: always
    volumes:
     - ./openvpn-data/conf:/etc/openvpn" > ./docker-compose.yml

tput setaf 11 
echo -e "\n(3/7) >>> docker-compose run --rm openvpn ovpn_genconfig -u udp://$1\n"
tput setaf 7 

docker-compose run --rm openvpn ovpn_genconfig -u udp://$1

tput setaf 11 
echo -e "\n(4/7) >>> docker-compose run --rm openvpn ovpn_initpki\n"
tput setaf 7 

docker-compose run --rm openvpn ovpn_initpki

tput setaf 11 
echo -e "\n(5/7) >>> sudo chown -R pi: ./openvpn-data\n"
tput setaf 7 

sudo chown -R pi: ./openvpn-data

tput setaf 11 
echo -e "\n(6/7) >>> docker-compose up -d openvpn\n"
tput setaf 7 

docker-compose up -d openvpn

tput setaf 11 
echo -e "\n(7/7) >>> docker-compose logs -f\n"
tput setaf 7 

docker-compose logs -f