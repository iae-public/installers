# NODE 12, NPM
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get -y dist-upgrade
sudo apt-get install -y build-essential libudev-dev git python-dev nodejs
sudo npm i -g npm pm2
