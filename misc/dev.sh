# NODE 12, NPM, TYPESCRIPT, @ANGULAR/CLI, IONIC, FIREBASE-TOOLS, DOCKER
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get -y dist-upgrade
sudo apt-get install -y build-essential libudev-dev git python-dev nodejs
sudo npm i -g npm typescript @angular/cli ionic firebase-tools
curl -sL  https://get.docker.com | sudo -E bash -
