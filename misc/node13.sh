# NODE 12, NPM
sudo apt update
sudo apt install -y build-essential curl libudev-dev git python-dev
curl -sL https://deb.nodesource.com/setup_13.x | sudo -E bash -
sudo apt -y dist-upgrade
sudo apt install -y nodejs
sudo npm i -g npm
