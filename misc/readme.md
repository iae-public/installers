**NODE 12, NPM, TYPESCRIPT, @ANGULAR/CLI, IONIC, FIREBASE-TOOLS, DOCKER**

`curl -sL https://gitlab.com/iae-public/installers/raw/master/misc/dev.sh | sudo -E bash -`

**NODE 10 & NPM**

`curl -sL https://gitlab.com/iae-public/installers/raw/master/misc/node10.sh | sudo -E bash -`

**NODE 12 & NPM**

`curl -sL https://gitlab.com/iae-public/installers/raw/master/misc/node12.sh | sudo -E bash -`

**NODE 13 & NPM**

`curl -sL https://gitlab.com/iae-public/installers/raw/master/misc/node13.sh | sudo -E bash -`

**NODE 22 & NPM**

`curl -sL https://gitlab.com/iae-public/installers/raw/master/misc/node122.sh | sudo -E bash -`