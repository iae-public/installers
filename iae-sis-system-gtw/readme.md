### Prepare
- Download raspbian lite from [raspberrypi.org](https://www.raspberrypi.org/downloads/raspbian/).
- Create SD using [win32diskimager](https://sourceforge.net/projects/win32diskimager/) or [etcher](https://etcher.io/).
- Add `ssh` empty file in `boot` partition.
- Expand file system using `rasp-config`.
- Enable serial hardware visible using `rasp-config`.
- Reboot using `sudo reboot`.
- Start **install** script.

### Install
```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/iae-sis-system-gtw/install.sh | sudo -E bash -s {DEVICE-GUID} {Repository-PAT}
```

**Repository-PAT**: Repository Personal Access Token


### Update
```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/iae-sis-system-gtw/update.sh | sudo -E bash -
```

### Update Only Project (no dependecies changed)
```sh 
curl -sL https://gitlab.com/iae-public/installers/raw/master/iae-sis-system-gtw/update.only.prj.sh | sudo -E bash -
```