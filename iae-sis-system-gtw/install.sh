tput setaf 3 
echo -e "\n(1/5) >>> SETUP SYSTEM\n"
tput setaf 7 

# Update packages, add nodejs repo and upgrade OS
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
sudo apt-get -y dist-upgrade

# install apt dependencies
sudo apt-get install -y build-essential libudev-dev git python-dev pigpio mosquitto jq nodejs
sudo npm i -g npm typescript pm2

# Configure programs
sudo echo "# by FV@iae
pid_file /var/run/moquitto.pid

port 1883

persistence false
log_type none

listener 9001
protocol websockets" > /etc/mosquitto/mosquitto.conf

tput setaf 3
echo -e "\n(2/5) >>> DOWNLOAD PROJECT\n"
tput setaf 7 

cd /home/pi/
# download source code from git as root
# sudo git clone git@gitlab.com:iae-sis-system/sis-system-gateway.git 
sudo git clone https://gitlab-ci-token:$2@gitlab.com/iae-sis-system/sis-system-gateway.git

# resore ownership as pi
sudo chown -R pi sis-system-gateway

cd sis-system-gateway

tput setaf 3
echo -e "\n(3/5) >>> INSTALL DEPENDENCIES\n"
tput setaf 7 

sudo npm i --unsafe-perm

sudo tput setaf 3
sudo echo -e "\n(4/5) >>> BUILD PROJECT\n"
sudo tput setaf 7 

sudo tsc

# create local data in build and configure json
mkdir -p build/local-data
jq ".deviceUid = \"$1\"" <<< `cat defaults/default.json` > build/local-data/config.json
jq ".deviceName = \"`hostname`\"" <<<`cat build/local-data/config.json` > build/local-data/config.json

tput setaf 3
echo -e "\n(5/5) >>> SETUP PM2\n"
tput setaf 7 

# start processes with pm2 as root (root pm2 group)
sudo pm2 start build/device-manager-process.js --name SIG-DM
sudo pm2 start build/main-process.js --name SIG-MAIN
sudo pm2 start build/publisher-process.js --name SIG-PUB
sudo pm2 start build/webui-process.js --name SIG-WEBUI
sudo pm2 start build/upd-process.js --name SIG-UPD
sudo tput setab 0 # riporto il bg a nero

# configure pm2 startup
sudo pm2 startup
sudo pm2 save

tput setab 0 # riporto il bg a nero
tput setaf 5
echo -e "\nGATEWAY STARTED WITH GUID: $1" 
echo -e "THEN RESTART SYSTEM ( sudo reboot )" 
tput setaf 7 
