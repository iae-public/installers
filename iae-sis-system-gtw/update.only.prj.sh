sudo echo -e "<1/3> UPDATE PROJECT"

cd /home/pi/sis-system-gateway
##sudo git config credential.helper store ## non serve se clono con il token
sudo git pull

sudo echo -e "<2/3> COMPILE PROJECT"

sudo tsc --build tsconfig.json

sudo echo -e "<3/3> RESTART PROCESS"

sudo pm2 restart SIG-DM SIG-MAIN SIG-PUB SIG-WEBUI

sudo echo -e "<DONE>" 
