tput setaf 11 
echo -e "\n(1/9) >>> sudo apt-get update\n"
tput setaf 7 

sudo apt-get update

tput setaf 11 
echo -e "\n(2/9) >>> sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common\n"
tput setaf 7 

sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common

tput setaf 11 
echo -e "\n(3/9) >>> curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -\n"
tput setaf 7 

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

tput setaf 11 
echo -e "\n(4/9) >>> sudo apt-key fingerprint 0EBFCD88\n"
tput setaf 7 

sudo apt-key fingerprint 0EBFCD88

tput setaf 11 
echo -e "\n(5/9) >>> sudo add-apt-repository \"deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\"\n"
tput setaf 7 

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"

tput setaf 11 
echo -e "\n(6/9) >>> sudo apt-get update\n"
tput setaf 7 

sudo apt-get update

tput setaf 11 
echo -e "\n(7/9) >>> sudo apt-get install -y docker-ce docker-ce-cli containerd.io\n"
tput setaf 7 

sudo apt-get install -y docker-ce docker-ce-cli containerd.io

tput setaf 11 
echo -e "\n(8/9) >>> sudo usermod -aG docker $1\n"
tput setaf 7 

sudo usermod -aG docker $USER

tput setaf 11 
echo -e "\n(9/9) >>> sudo apt install -y docker-compose\n"
tput setaf 7 

sudo apt install -y docker-compose

tput setaf 5 
echo -e "\nRESTART SYSTEM ( sudo reboot )\n" 
tput setaf 7 
