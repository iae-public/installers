**SERVER (x86, x64)**

`curl -sL https://gitlab.com/iae-public/installers/raw/master/docker/install.sh | sudo -E bash -`

se ad esempio siete loggati con l'utente "pippo" eseguite il comando:

`curl -sL https://gitlab.com/iae-public/installers/raw/master/docker/install.sh | sudo -E bash -`

**RASPBERRY (ARM)**

`curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh`

poi per evitare di dover usare **sudo** ogni volta (con docker)

`sudo usermod -aG docker pi`

A questo punto occorre **riloggarsi**

**oppure**

`curl -sL https://gitlab.com/iae-public/installers/raw/master/docker/rpi.sh | sudo -E bash -`
