sudo tput setaf 3
sudo echo -e "\n(1/3) >>> UPDATE PROJECT\n"
sudo tput setaf 7

cd /usr/bin/iae-fs-gtw
sudo git pull

sudo tput setaf 3
sudo echo -e "\n(2/3) >>> COMPILE PROJECT\n"
sudo tput setaf 7

sudo tsc

sudo tput setaf 3
sudo echo -e "\n(3/3) >>> RESTART PROCESS\n"
sudo tput setaf 7

sudo pm2 restart iae-fs-gtw
sudo tput setab 0 # riporto il bg a nero

sudo tput setaf 5
sudo echo -e "\nDONE" 
sudo tput setaf 7

sudo pm2 monit